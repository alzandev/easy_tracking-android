**Easy Tracking | Rastreio de Encomendas**


Gerencie e acompanhe suas encomendas dos Correios em um s� lugar.


Recursos: 

 - Acompanhe sua encomenda da postagem a entrega.
 - Tela especial para encomendas: (Entregues, Em Andamento e/ou Arquivadas).
 - Suporta todos os servi�os de encomenda dos Correios, Nacionais e Internacionais (PAC, SEDEX, SEDEX 10,...).
 - Interface intuitiva e simples que facilita o rastreio.
 
https://play.google.com/store/apps/details?id=com.alzan.easytracking 