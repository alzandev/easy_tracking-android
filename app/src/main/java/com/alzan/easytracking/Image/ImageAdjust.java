package com.alzan.easytracking.Image;

/**
 * Created by Calazans on 26/08/2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ImageAdjust extends ImageView {

    public ImageAdjust(Context context) {
        super(context);
    }

    public ImageAdjust(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageAdjust(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = getDrawable();
        if (d != null) {
            int w = MeasureSpec.getSize(widthMeasureSpec);
            int h = w * d.getIntrinsicHeight() / d.getIntrinsicWidth();
            setMeasuredDimension(w, h);
        }
        else super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}