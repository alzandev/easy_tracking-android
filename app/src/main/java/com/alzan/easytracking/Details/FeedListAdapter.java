package com.alzan.easytracking.Details;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.alzan.easytracking.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import kotlin.text.Charsets;

public class FeedListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    public static final String TAG = FeedListAdapter.class.getSimpleName();
    public static String URL_FEED;
    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName("UTF-8");

    public FeedListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.feed_item, null);

        TextView rastreio = (TextView) convertView.findViewById(R.id.rastreio);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);

        FeedItem item = feedItems.get(position);

        Pattern UCODE_PATTERN = Pattern.compile("\\\\u[0-9a-fA-F]{4}");

        String statusFinal = item.getLocal();

//        try {
//            String statusToConvert = item.getLocal();
//            byte[] iso88591Data = statusToConvert.getBytes("ISO-8859-1");
//            statusFinal = new String(iso88591Data);
//
//            Log.d("FUCK THE SHIT", String.valueOf(statusFinal));
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        status.setText(statusFinal);

//        rastreio.setText(item.getStatus());
//        status.setText(item.getLocal());
        time.setText(item.getData());


        String image = item.getStatus();
        String status1 = item.getStatus();

        String local1 = item.getLocal();
        String localNull = item.getLocalNull();
        try {
            byte[] iso88591Data = image.getBytes("ISO-8859-1");
            String text = new String(iso88591Data);
            String str2 = new String(text);

            byte[] localNullISO = image.getBytes("ISO-8859-1");
            String localNulLText = new String(localNullISO);

            String localNulLFinal = new String(localNulLText);


//            Log.d("TAG", str2);

//            rastreio.setText(new String(utf8, "UTF-8"));
//            status.setText(local1);
//            time.setText(item.getData());

            if (str2.equals("Objeto postado")) {
                rastreio.setText("Postado");
            } else if (str2.equals("Objeto encaminhado")) {
                rastreio.setText("Encaminhado");
            } else if (str2.equals("Objeto saiu para entrega ao destinatário")
                    || str2.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio")
                    || str2.equals("Objeto saiu para entrega ao destinat�rio")) {
                rastreio.setText("Saiu para entrega ao destinatário");
            } else if (str2.equals("Objeto entregue ao destinatário") || str2.equals("Objeto entregue ao destinatário? ?")
                    || str2.equals("Objeto entregue ao destinatário? ?") || str2.equals("Objeto entregue ao destinat�rio")) {
                rastreio.setText("Entrega Efetuada");
            } else if (str2.equals("Objeto aguardando retirada no endereço indicado")) {
                rastreio.setText("Aguardando retirada");
            } else if (str2.equals("Objeto recebido pelos Correios do Brasil")) {
                rastreio.setText("Recebido pelos Correios do Brasil");
            } else if (str2.equals("�rea com distribui��o sujeita a prazo diferenciado")) {
                rastreio.setText("Área com distribuição sujeita a prazo diferenciado");
            } else if (str2.equals("Objeto postado após o horário limite da ag�ncia")
                    || str2.equals("Objeto postado após o horário limite da agência")
                    || str2.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
                rastreio.setText("Postado após o horário limite da agência");
            } else if (str2.equals("Objeto ainda n�o chegou � unidade")
                    || str2.equals("Objeto ainda não chegou à unidade")
                    || str2.equals("Objeto ainda n�o chegou � unidade.")) {
                rastreio.setText("Objeto ainda não chegou à unidade");
            }else if (str2.equals("Fiscalização aduaneira finalizada")
                    || str2.equals("Fiscaliza��o aduaneira finalizada")) {
                rastreio.setText("Fiscalização aduaneira finalizada");
            }else if (str2.equals("Objeto recebido na unidade de exporta��o")
                    || str2.equals("Objeto recebido na unidade de exportação")) {
                rastreio.setText("Objeto recebido na unidade de exportação");
            } else {
                rastreio.setText(str2);
            }

            if (local1.equals("")) {
                status.setText(localNull);
            }

            if (str2.equals("Objeto postado")) {
                imageView.setImageResource(R.drawable.postado);
            } else if (str2.equals("Objeto encaminhado")) {
                imageView.setImageResource(R.drawable.encaminhado);
            } else if (str2.equals("Objeto saiu para entrega ao destinatário")
                    || str2.equals("Objeto saiu para entrega ao destinat�rio")) {
                imageView.setImageResource(R.drawable.saiu);
            } else if (str2.equals("Objeto entregue ao destinatário") || str2.equals("Objeto entregue ao destinatário? ?")
                    || str2.equals("Objeto entregue ao destinat�rio")) {
                imageView.setImageResource(R.drawable.success);
            } else if (str2.equals("Objeto aguardando retirada no endereço indicado")) {
                imageView.setImageResource(R.drawable.wait);
            } else if (str2.equals("Objeto recebido pelos Correios do Brasil")) {
                imageView.setImageResource(R.drawable.brasil);
            } else if (str2.equals("Destinatário Ausente")
                    || str2.equals("Destinatário não apresentou-se para receber")
                    || str2.equals("Destinatário recusou-se a receber")
                    || str2.equals("Destinatário desconhecido")
                    || str2.equals("Destinatário mudou-se")
                    || str2.equals("Destinatário não apresentou-se para receber")
                    || str2.equals("Destinatário ausente")
                    || str2.equals("Destinatário ausente em 3 tentativas de entrega")
                    || str2.equals("Destinatário não apresentou a documentação")) {
                imageView.setImageResource(R.drawable.nao_entregue);
            } else if (str2.equals("Objeto postado após o horário limite da agência")
                    || str2.equals("Objeto postado ap�s o horário limite da ag�ncia")
                    || str2.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
                imageView.setImageResource(R.drawable.time);
            } else {
                imageView.setImageResource(R.drawable.null_reference);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convertView;
    }
}
