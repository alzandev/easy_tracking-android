package com.alzan.easytracking.Details;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.SubtitleCollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alzan.easytracking.AppController;
import com.alzan.easytracking.R;
import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.R.attr.offset;

public class ScrollingActivity extends AppCompatActivity {

    private static String URL_FEED;

    public static final String TAG = ScrollingActivity.class.getSimpleName();
    public ListView listView;
    public FeedListAdapter listAdapter;
    public List<FeedItem> feedItems;
    public static String codRastreio;
    public static String rastreioTitle;
    public int REQ_CODE = 1;
    SwipeRefreshLayout mSwipeRefreshLayout;
//    public ProgressBar progressBar;

    public String getCodRastreio() {
        return codRastreio;
    }

    public static void setCodRastreio(String codRastreio) {
        ScrollingActivity.codRastreio = codRastreio;
    }

    public String getRastreioTitle() {
        return rastreioTitle;
    }

    public static void setRastreioTitle(String rastreioTitle) {
        ScrollingActivity.rastreioTitle = rastreioTitle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.list);
        listView.setNestedScrollingEnabled(true);
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        SubtitleCollapsingToolbarLayout toolbar_layout = (SubtitleCollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        if (rastreioTitle.equals("") || rastreioTitle.equals(null)) {
            toolbar_layout.setTitle(codRastreio);
        } else {
            toolbar_layout.setTitle(rastreioTitle);
            toolbar_layout.setSubtitle(codRastreio);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);

//        progressBar = (ProgressBar) findViewById(R.id.progressBar);

//        listView.setEmptyView(findViewById(R.id.empty));

//        listView.setEmptyView(findViewById(R.id.empty));


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                listView.setAdapter(null);
                listView(URL_FEED);
                refreshList();
            }
        });

//        getSupportActionBar().setTitle(codRastreio);
//        getSupportActionBar().setSubtitle(codRastreio);


//        String eventId = FirebaseDatabase.getInstance().getReference().child("rastreio").push()
//                .getKey();
//
//        FirebaseDatabase.getInstance().getReference().child(Build.MODEL).child(eventId).setValue(codRastreio);

        if (codRastreio != null) {
            URL_FEED = "http://api.postmon.com.br/v1/rastreio/ect/" + codRastreio;
            listView(URL_FEED);
        } else {
        }
    }


    void refreshList() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void listView(String URL_FEED) {

        feedItems = new ArrayList<FeedItem>();
        listAdapter = new FeedListAdapter(this, feedItems);
        listView.setAdapter(listAdapter);

        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");

                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json

            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        try {
                            parseJsonFeed(response);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    listView.setEmptyView(findViewById(R.id.empty));
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    // fake a network operation's delayed response
    // this is just for demonstration, not real code!

    /**
     * Parsing json reponse and passing the data to feed view list adapter
     */

    private void parseJsonFeed(JSONObject response) throws UnsupportedEncodingException {
        try {

//            progressBar.setMax(100);
//            progressBar.setProgress(1);
            JSONArray feedArray = response.getJSONArray("historico");

            for (int i = feedArray.length() - 1; i >= 0; i--) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();

                String detalhes = feedObj.isNull("detalhes") ? null : feedObj.getString("detalhes");
                item.setLocal(detalhes);
                Log.d("01101997", detalhes);

                String data = feedObj.isNull("data") ? null : feedObj.getString("data");
                item.setData(data);

                String situacao = feedObj.isNull("situacao") ? null : feedObj.getString("situacao");
                item.setStatus(situacao);

                String localnull = feedObj.isNull("local") ? null : feedObj.getString("local");
                item.setLocalNull(localnull);

                feedItems.add(item);
            }
//
//            progressBar.setProgress(100);
//            progressBar.setVisibility(View.GONE);


            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            trimCache(getBaseContext());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

}
