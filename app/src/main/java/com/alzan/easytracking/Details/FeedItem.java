package com.alzan.easytracking.Details;

public class FeedItem {
    private String id;
    private String local, data, status, localNull;

    public FeedItem() {
    }

    public FeedItem(String local, String data, String status, String localNull) {
        super();
        this.local = local;
        this.data = data;
        this.status = status;
        this.localNull = localNull;
    }
    public String getLocalNull() {
        return localNull;
    }

    public void setLocalNull(String localNull) {
        this.localNull = localNull;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}