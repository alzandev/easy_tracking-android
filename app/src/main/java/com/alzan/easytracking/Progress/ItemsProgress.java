package com.alzan.easytracking.Progress;

public class ItemsProgress {
    String title;
    String code;
    String status1;
    String fav;

    public ItemsProgress(String color, String name, String status1) {
        this.code = color;
        this.title = name;
        this.status1 = status1;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String color) {
        this.code = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String place) {
        this.status1 = place;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

}