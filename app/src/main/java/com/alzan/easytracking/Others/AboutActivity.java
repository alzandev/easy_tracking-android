package com.alzan.easytracking.Others;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.alzan.easytracking.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AboutActivity extends AppCompatActivity implements RewardedVideoAdListener {

    private CardView facebook;
    private CardView site;
    private CardView mail;
//    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mAd;
    private AdView adView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        setTitle("Sobre");

        facebook = (CardView) findViewById(R.id.cardFacebook);
        mail = (CardView) findViewById(R.id.mail);
        site = (CardView) findViewById(R.id.cardSite);

//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId("ca-app-pub-3657674781649044/7498838730");
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());

//        NativeExpressAdView adView = (NativeExpressAdView) findViewById(R.id.adView);
//        AdRequest request = new AdRequest.Builder().build();
//        adView.loadAd(request);

        String ADS_ID = "ca-app-pub-3657674781649044~7396441548";

        MobileAds.initialize(this, ADS_ID);



        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


//        loadRewardedVideoAd();
//
//        if (mAd.isLoaded()) {
//            mAd.show();
//        }

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://www.facebook.com/AlzanDev"));
                startActivity(i);
            }
        });

        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showRewardedVideo();
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("http://alzan.com.br/"));
                startActivity(i);
            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "williancalazans8@gmail.com", null));
                i.putExtra(Intent.EXTRA_SUBJECT, "Easy Tracking | Rastreio Fácil dos Correios");
                startActivity(i.createChooser(i, "Abrir com"));
            }
        });



    }

    private void showRewardedVideo() {
        if (mAd.isLoaded()) {
            mAd.show();
        }
    }

    private void loadRewardedVideoAd() {
        mAd.loadAd("ca-app-pub-3657674781649044/2180322626", new AdRequest.Builder().build());
    }

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }
}
