package com.alzan.easytracking.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alzan.easytracking.DBController;
import com.alzan.easytracking.Entregues.AdapterEntregues;
import com.alzan.easytracking.Entregues.ItemsEntregues;
import com.alzan.easytracking.R;
import com.alzan.easytracking.Rastreio.AdapterMain;

import java.util.ArrayList;


public class CompleteFragment extends Fragment {
    public static CompleteFragment newInstance() {
        CompleteFragment fragment = new CompleteFragment();
        return fragment;
    }

    private static final String COMMA_SEP = ",";
    static DBController dbController;

    public static AdapterEntregues myCustomAdapter = null;
    static ListView listView = null;
    static ArrayList<ItemsEntregues> cars = null;
    private static Context context = null;
    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complete_fragment, container, false);

        dbController = new DBController(view.getContext());
        listView = (ListView) view.findViewById(R.id.list);

        context = getActivity();

        listView.setEmptyView(view.findViewById(R.id.empty));

        listView();

        final CoordinatorLayout coorLayout = (CoordinatorLayout) view.findViewById(R.id.coorLayout);

        myCustomAdapter.notifyDataSetChanged();

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                listView.setAdapter(null);
                listView();
                refreshList();
            }
        });

        return view;
    }

    public static void removed(int result) {
        ArrayAdapter arrayAdapter = myCustomAdapter;
        if (result == 1) {
            myCustomAdapter.notifyDataSetChanged();
            myCustomAdapter.clear();
            dbController = new DBController(context);
            myCustomAdapter = new AdapterEntregues(context, R.layout.rastreio_feed_item, cars);
            cars = dbController.completeData();
            listView.setAdapter(myCustomAdapter);
        } else {
        }
    }

    void refreshList() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


    public void listView() {
        try {

            dbController = new DBController(getActivity());
            cars = dbController.completeData();
            myCustomAdapter = new AdapterEntregues(getActivity(), R.layout.favorite_feed_item, cars);

            listView.setAdapter(myCustomAdapter);

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
