package com.alzan.easytracking.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alzan.easytracking.AppController;
import com.alzan.easytracking.DBController;
import com.alzan.easytracking.Details.FeedItem;
import com.alzan.easytracking.R;
import com.alzan.easytracking.Rastreio.AdapterMain;
import com.alzan.easytracking.Rastreio.Items;
import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azimolabs.maskformatter.MaskFormatter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class RastreioFragment extends Fragment {

    private static final String COMMA_SEP = ",";
    static DBController dbController;

    public static AdapterMain myCustomAdapter = null;
    static ListView listView = null;
    static ArrayList<Items> cars = null;
    private static Context context = null;
    public static Context baseContext;
    ProgressDialog progressDialog;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AdView adView;
    private RewardedVideoAd rewardedVideoAd;
    private static final String IBAN_MASK = "AA999999999AA";


    public static final String TAG = RastreioFragment.class.getSimpleName();

    public static RastreioFragment newInstance() {
        RastreioFragment fragment = new RastreioFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rastreio_fragment, container, false);

        baseContext = getActivity();

        dbController = new DBController(view.getContext());
        listView = (ListView) view.findViewById(R.id.list);

        context = getActivity();

        listView.setEmptyView(view.findViewById(R.id.empty));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                listView.setAdapter(null);
                listView();
                refreshList();
            }
        });

        String ADS_ID = "ca-app-pub-3657674781649044~7396441548";
        MobileAds.initialize(getActivity(), ADS_ID);

        listView();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.floatingActionButton);
        final CoordinatorLayout coorLayout = (CoordinatorLayout) view.findViewById(R.id.coorLayout);

//        myCustomAdapter.notifyDataSetChanged();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                builder.setView(inflater.inflate(R.layout.rastreio_alert_dialog, null))
                        .setPositiveButton("ADICIONAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                ProgressDialog progressDialog = new ProgressDialog(getContext());
                                progressDialog.setTitle("Adicionando Objeto...");
                                progressDialog.setMessage("aguarde...");
                                progressDialog.show();

                                Dialog dialog1 = (Dialog) dialog;
                                DBController crud = new DBController(v.getContext());
                                String resultado;
                                String resultado2;

                                TextInputLayout til = (TextInputLayout) dialog1.findViewById(R.id.tiLayout);
                                EditText cod = (EditText) dialog1.findViewById(R.id.cod);
                                EditText ident = (EditText) dialog1.findViewById(R.id.ident);

//                                String ADS_ID = "ca-app-pub-3657674781649044~7396441548";
//                                MobileAds.initialize(getActivity(), ADS_ID);
//
//                                adView = (AdView) dialog1.findViewById(R.id.adView);
//                                AdRequest adRequest = new AdRequest.Builder().build();
//                                adView.loadAd(adRequest);

//                                String eventId = FirebaseDatabase.getInstance().getReference().child("rastreio").push()
//                                        .getKey();

//                                FirebaseDatabase.getInstance().getReference().child(Build.MODEL).child(eventId).setValue(cod.getText().toString());

                                resultado = crud.insertData(cod.getText().toString().toUpperCase(), ident.getText().toString());

                                if (resultado.equals("UNIQUE constraint failed: objetos.codigo (code 2067)")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setTitle("Objeto já adicionado!");
                                    builder.setMessage("Verifique a lista de objetos ou o codigo digitado");
                                    builder.show();
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Snackbar snackbar = Snackbar.make(v, "Objeto Adicionado!", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }

                                listView.setAdapter(null);
                                listView();
                                refreshList();
                            }
                        }).setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });


                final AlertDialog dialog = builder.create();
                dialog.show();
                final EditText cod = (EditText) dialog.findViewById(R.id.cod);
                cod.addTextChangedListener(new MaskFormatter(IBAN_MASK, cod));

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

                cod.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (count == 0) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }
        });


//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        String date = sdf.format(new Date());
//
//        SimpleDateFormat sdt = new SimpleDateFormat("HH:mm:ss");
//        String time = sdt.format(new Date());

//        Toast.makeText(getContext(), date + " " + time, Toast.LENGTH_LONG).show();

//        try {
//            Thread.sleep(59900);
//            listView();
//        }catch (Exception ex){
//            Toast.makeText(getContext(), "5125125: " + ex.getMessage(), Toast.LENGTH_LONG).show();
//        }

        return view;
    }

    private void loadRewardedVideoAd() {
        rewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder().build());
    }

    void refreshList() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

//    public static void removed(int result) {
//        ArrayAdapter arrayAdapter = myCustomAdapter;
//        if (result == 1) {
//             myCustomAdapter.notifyDataSetChanged();
//            myCustomAdapter.clear();
//            dbController = new DBController(context);
//            cars = dbController.getData();
//            myCustomAdapter = new AdapterMain(context, R.layout.rastreio_feed_item, cars);
//            cars = dbController.getData();
//            listView.setAdapter(myCustomAdapter);
//        } else {
//        }
//    }

    public void listView() {
        try {

            dbController = new DBController(getActivity());
            cars = dbController.getData();
            myCustomAdapter = new AdapterMain(getActivity(), R.layout.rastreio_feed_item, cars);

            listView.setAdapter(myCustomAdapter);

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static void listViewRefresh() {
        try {

            dbController = new DBController(context);
            cars = dbController.getData();
            myCustomAdapter = new AdapterMain(context, R.layout.rastreio_feed_item, cars);

            listView.setAdapter(myCustomAdapter);

        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static void listViewJSON() {

        String URL = "http://api.postmon.com.br/v1/rastreio/ect/" + AdapterMain.object.getCode();

        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    Toast.makeText(baseContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(baseContext, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json

            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public static String situacao66 = null;


    private static void parseJsonFeed(JSONObject response) {
        try {

            JSONArray feedArray = response.getJSONArray("historico");


            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdt = new SimpleDateFormat("HH:mm");

            String finalDate = sdf.format(new Date()) + " " + sdt.format(new Date());

            int i;

            for (i = feedArray.length() - 1; i >= feedArray.length() - 1; i--) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();

                situacao66 = feedObj.isNull("situacao") ? null : feedObj.getString("situacao");
                String codigo = response.getString("codigo");
                String data = feedObj.getString("data");

//                if(data.equals(finalDate)){
//                    Toast.makeText(context, "OK - NOTIFY", Toast.LENGTH_LONG).show();
//                    Notification();
//                } else {
//                  Toast.makeText(context, "FALSE - NOTIFY | " + data, Toast.LENGTH_LONG).show();
//                }

                try {
                    byte[] latin1 = situacao66.getBytes("ISO-8859-1");
                    byte[] utf8 = new String(latin1, "UTF-8").getBytes("UTF-8");

                    String str2 = new String(utf8, "UTF-8");

//                    Log.d("TAG", str2);

                    String finalSTR = situacao66;

                    if (str2.equals("Objeto postado")) {
                        finalSTR = ("Postado");
                    } else if (str2.equals("Objeto encaminhado")) {
                        finalSTR = ("Encaminhado");
                    } else if (str2.equals("Objeto saiu para entrega ao destinatário")
                            || str2.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio")
                            || str2.equals("Objeto saiu para entrega ao destinat�rio")) {
                        finalSTR = ("Saiu para entrega ao destinatário");
                    } else if (str2.equals("Objeto entregue ao destinatário") || str2.equals("Objeto entregue ao destinatário? ?")
                            || str2.equals("Objeto entregue ao destinatário? ?") || str2.equals("Objeto entregue ao destinat�rio")) {
                        finalSTR = ("Entrega Efetuada");
                    } else if (str2.equals("Objeto aguardando retirada no endereço indicado")) {
                        finalSTR = ("Aguardando retirada");
                    } else if (str2.equals("Objeto recebido pelos Correios do Brasil")) {
                        finalSTR = ("Recebido pelos Correios do Brasil");
                    } else if (str2.equals("�rea com distribui��o sujeita a prazo diferenciado")) {
                        finalSTR = ("Área com distribuição sujeita a prazo diferenciado");
                    } else if (str2.equals("Objeto postado após o horário limite da ag�ncia")
                            || str2.equals("Objeto postado após o horário limite da agência")
                            || str2.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
                        finalSTR = ("Postado após o horário limite da agência");
                    } else if (str2.equals("Objeto ainda n�o chegou � unidade")
                            || str2.equals("Objeto ainda não chegou à unidade")
                            || str2.equals("Objeto ainda n�o chegou � unidade.")) {
                        finalSTR = ("Objeto ainda não chegou à unidade");
                    } else if (str2.equals("Fiscalização aduaneira finalizada")
                            || str2.equals("Fiscaliza��o aduaneira finalizada")) {
                        finalSTR = ("Fiscalização aduaneira finalizada");
                    } else if (str2.equals("Objeto recebido na unidade de exporta��o")
                            || str2.equals("Objeto recebido na unidade de exportação")) {
                        finalSTR = ("Recebido na unidade de exportação");
                    }

                    long timeInMillis = System.currentTimeMillis();
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTimeInMillis(timeInMillis);
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy hh:mm:ss a");

//                    Log.d("TAG UPDATE " + dateFormat.format(cal1.getTime()), finalSTR);
                    dbController.updateStatus(finalSTR, codigo, data);
//
                    //Notification
//
//                    if (data.equals(dbController.getUnique())) {
//                    } else {
//                        dbController.updateStatus(finalSTR, codigo, data);
//
//                        if (str2.equals("Objeto postado")) {
//                            notification("Postado", codigo);
//                        } else if (str2.equals("Objeto encaminhado")) {
//                            notification("Encaminhado", codigo);
//                        } else if (str2.equals("Objeto saiu para entrega ao destinatário")
//                                || str2.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio")
//                                || str2.equals("Objeto saiu para entrega ao destinat�rio")) {
//                            notification("Saiu%20para%20entrega%20ao%20destinatário", codigo);
//                        } else if (str2.equals("Objeto entregue ao destinatário") || str2.equals("Objeto entregue ao destinatário? ?")
//                                || str2.equals("Objeto entregue ao destinatário? ?") || str2.equals("Objeto entregue ao destinat�rio")) {
//                            notification("Entrega%20Efetuada", codigo);
//                        } else if (str2.equals("Objeto aguardando retirada no endereço indicado")) {
//                            notification("Aguardando%20retirada", codigo);
//                        } else if (str2.equals("Objeto recebido pelos Correios do Brasil")) {
//                            notification("Recebido%20pelos%20Correios%20do%20Brasil", codigo);
//                        } else if (str2.equals("Objeto postado após o horário limite da ag�ncia")
//                                || str2.equals("Objeto postado após o horário limite da agência")
//                                || str2.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
//                            notification("Postado%20após%20o%20horário%20limite%20da%20agência", codigo);
//                        } else {
//                            notification(str2, codigo);
//                        }
//                        Log.d("DB RESULT", data);
//                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            // notify data changes to list adapater
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(baseContext, e.getMessage(), Toast.LENGTH_LONG).show();

        } finally {
//            listViewRefresh();
        }
    }
}
