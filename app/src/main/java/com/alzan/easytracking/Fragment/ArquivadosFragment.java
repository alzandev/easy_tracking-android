package com.alzan.easytracking.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.alzan.easytracking.AppController;
import com.alzan.easytracking.DBController;
import com.alzan.easytracking.Details.FeedItem;
import com.alzan.easytracking.Archive.AdapterArch;
import com.alzan.easytracking.Archive.ItemsArch;
import com.alzan.easytracking.Entregues.AdapterEntregues;
import com.alzan.easytracking.R;
import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class ArquivadosFragment extends Fragment {

    private static final String COMMA_SEP = ",";
    static DBController dbController;

    public static AdapterArch myCustomAdapter = null;
    static ListView listView = null;
    static ArrayList<ItemsArch> cars = null;
    private static Context context = null;
    SwipeRefreshLayout mSwipeRefreshLayout;


    static SimpleCursorAdapter adapter;

    public static final String TAG = ArquivadosFragment.class.getSimpleName();

    public static ArquivadosFragment newInstance() {
        ArquivadosFragment fragment = new ArquivadosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorite_fragment, container, false);

        dbController = new DBController(view.getContext());
        listView = (ListView) view.findViewById(R.id.list);

        context = getActivity();

        listView.setEmptyView(view.findViewById(R.id.empty));

        listView();


        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                listView.setAdapter(null);
                listView();
                refreshList();
            }
        });

        final CoordinatorLayout coorLayout = (CoordinatorLayout) view.findViewById(R.id.coorLayout);

        return view;
    }

    void refreshList() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


    public static void removed(int result) {
        ArrayAdapter arrayAdapter = myCustomAdapter;
        if (result == 1) {
            myCustomAdapter.notifyDataSetChanged();
            myCustomAdapter.clear();
            dbController = new DBController(context);
            cars = dbController.favoriteData();
            myCustomAdapter = new AdapterArch(context, R.layout.favorite_feed_item, cars);
            listView.setAdapter(myCustomAdapter);
        } else {
        }
    }


    public void listView() {
        try {

            dbController = new DBController(getActivity());
            cars = dbController.favoriteData();
            myCustomAdapter = new AdapterArch(getActivity(), R.layout.favorite_feed_item, cars);

            listView.setAdapter(myCustomAdapter);

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static void listViewJSON(String URL_FEED) {

        String URL = "http://api.postmon.com.br/v1/rastreio/ect/" + URL_FEED;

        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");

                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json

            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private static void parseJsonFeed(JSONObject response) {
        try {

            JSONArray feedArray = response.getJSONArray("historico");

            int i;

            for (i = feedArray.length() - 1; i >= feedArray.length() - 1; i--) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();

                String situacao = feedObj.isNull("situacao") ? null : feedObj.getString("situacao");
                String codigo = response.getString("codigo");
                String data = feedObj.getString("data");


                dbController.updateStatus(situacao, codigo, data);

            }

            // notify data changes to list adapater
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }
}
