package com.alzan.easytracking.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alzan.easytracking.DBController;
import com.alzan.easytracking.Entregues.AdapterEntregues;
import com.alzan.easytracking.Progress.AdapterProgress;
import com.alzan.easytracking.Progress.ItemsProgress;
import com.alzan.easytracking.R;
import com.alzan.easytracking.Rastreio.AdapterMain;

import java.util.ArrayList;


public class ProgressFragment extends Fragment {

    private static final String COMMA_SEP = ",";
    static DBController dbController;

    public static AdapterProgress myCustomAdapter = null;
    static ListView listView = null;
    static ArrayList<ItemsProgress> cars = null;
    private static Context context = null;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public static ProgressFragment newInstance() {
        ProgressFragment fragment = new ProgressFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.progress_fragment, container, false);

        dbController = new DBController(view.getContext());
        listView = (ListView) view.findViewById(R.id.list);
        context = getActivity();
        listView.setEmptyView(view.findViewById(R.id.empty));

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                listView.setAdapter(null);
                listView();
                refreshList();
            }
        });

        listView();
        final CoordinatorLayout coorLayout = (CoordinatorLayout) view.findViewById(R.id.coorLayout);

        return view;

    }

    void refreshList() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


    public void listView() {
        try {

            dbController = new DBController(getActivity());
            cars = dbController.progressData();
            myCustomAdapter = new AdapterProgress(getActivity(), R.layout.progress_feed_item, cars);

            listView.setAdapter(myCustomAdapter);

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static void removed(int result) {
        ArrayAdapter arrayAdapter = myCustomAdapter;
        if (result == 1) {
            myCustomAdapter.notifyDataSetChanged();
            myCustomAdapter.clear();
            dbController = new DBController(context);
            cars = dbController.progressData();
            myCustomAdapter = new AdapterProgress(context, R.layout.progress_feed_item, cars);
            listView.setAdapter(myCustomAdapter);
        } else {
        }
    }

}
