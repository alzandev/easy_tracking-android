package com.alzan.easytracking.Rastreio;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alzan.easytracking.DBController;
import com.alzan.easytracking.Details.ScrollingActivity;
import com.alzan.easytracking.Fragment.RastreioFragment;
import com.alzan.easytracking.R;

import java.util.ArrayList;

//import static com.alzan.easytracking.Fragment.RastreioFragment.removed;


public class AdapterMain extends ArrayAdapter {
    private Context context;
    private ArrayList<Items> objectList;
    public static String status;
    public static Items object;
    static DBController dbController;
    public static final String TAG = AdapterMain.class.getSimpleName();

    public AdapterMain(Context context, int textViewResourceId, ArrayList objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        objectList = objects;

    }

    public String code;

    public String getCod() {
        return code;
    }

    public void setCod(String code) {
        this.code = code;
    }

    private class ViewHolder {
        TextView rastreio;
        TextView status;
        TextView time;
        ImageView imageView;
        CardView card;
        Button button;
    }


    public String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        AdapterMain.status = status;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.rastreio_feed_item, null);

            holder = new ViewHolder();
            holder.status = (TextView) convertView.findViewById(R.id.status);
            holder.rastreio = (TextView) convertView.findViewById(R.id.rastreio);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            holder.card = (CardView) convertView.findViewById(R.id.card);
            holder.button = (Button) convertView.findViewById(R.id.toggleButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        object = objectList.get(position);
        holder.rastreio.setText(object.getTitle());
        holder.status.setText(object.getCode());
        holder.time.setText(object.getStatus1());
        RastreioFragment.listViewJSON();
        holder.imageView.setImageResource(R.drawable.null_reference);


        final String finalstatus = object.getStatus1();

        dbController = new DBController(convertView.getContext());

        String favoriteResult = object.getFav();
        final String code = object.getCode();


        final ViewHolder finalHolder = holder;
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                PopupMenu popupMenu = new PopupMenu(getContext(), finalHolder.button);
                popupMenu.getMenuInflater().inflate(R.menu.menu_items, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.archive) {
                            dbController.updateFavorite("true", code);
                            Snackbar.make(v, "Objeto Arquivado", Snackbar.LENGTH_LONG).show();
//                            removed(1);
                            RastreioFragment.listViewRefresh();
                        } else if (id == R.id.apagar) {
                            delete(code, v);
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        final String titleRatro = object.getTitle();
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScrollingActivity.setCodRastreio(code);
                ScrollingActivity.setRastreioTitle(titleRatro);
                v.getContext().startActivity(new Intent(v.getContext(), ScrollingActivity.class));
            }
        });

        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                delete(code, v);
                return false;
            }
        });

        try {
            holder.time.setText(object.getStatus1());
            String str3 = object.getStatus1();

            if (str3.equals("Postado")) {
                holder.imageView.setImageResource(R.drawable.postado);
            } else if (str3.equals("Encaminhado")) {
                holder.imageView.setImageResource(R.drawable.encaminhado);
            } else if (str3.equals("Objeto saiu para entrega ao destinatário")
                    || str3.equals("Saiu para entrega ao destinatário")) {
                holder.imageView.setImageResource(R.drawable.saiu);
            } else if (str3.equals("Objeto entregue ao destinatário") || str3.equals("Objeto entregue ao destinatário? ?")
                    || str3.equals("Entrega Efetuada")) {
                holder.imageView.setImageResource(R.drawable.success);
            } else if (str3.equals("Aguardando retirada")) {
                holder.imageView.setImageResource(R.drawable.wait);
            } else if (str3.equals("Recebido pelos Correios do Brasil")) {
                holder.imageView.setImageResource(R.drawable.brasil);
            } else if (str3.equals("Destinatário Ausente")
                    || str3.equals("Destinatário não apresentou-se para receber")
                    || str3.equals("Destinatário recusou-se a receber")
                    || str3.equals("Destinatário desconhecido")
                    || str3.equals("Destinatário mudou-se")
                    || str3.equals("Destinatário não apresentou-se para receber")
                    || str3.equals("Destinatário ausente")
                    || str3.equals("Destinatário ausente em 3 tentativas de entrega")
                    || str3.equals("Destinatário não apresentou a documentação")) {
                holder.imageView.setImageResource(R.drawable.nao_entregue);
            } else if (str3.equals("Postado após o horário limite da agência")
                    || str3.equals("Objeto postado ap�s o horário limite da ag�ncia")
                    || str3.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
                holder.imageView.setImageResource(R.drawable.time);
            } else {
                holder.imageView.setImageResource(R.drawable.null_reference);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

//        try {
//
//            if (finalstatus.equals("Objeto postado")) {
//                holder.time.setText("Postado");
//            } else if (finalstatus.equals("Objeto encaminhado")) {
//                holder.time.setText("Encaminhado");
//            } else if (finalstatus.equals("Objeto saiu para entrega ao destinatário")
//                    || finalstatus.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio") ) {
//                holder.time.setText("Saiu para entrega ao destinatário");
//            } else if (finalstatus.equals("Objeto entregue ao destinatário") || finalstatus.equals("Objeto entregue ao destinatário? ?")
//                    || finalstatus.equals("Objeto entregue ao destinatário? ?")) {
//                holder.time.setText("Entrega Efetuada");
//            } else if (finalstatus.equals("Objeto aguardando retirada no endereço indicado")) {
//                holder.time.setText("Aguardando retirada");
//            } else if (finalstatus.equals("Objeto recebido pelos Correios do Brasil")) {
//                holder.time.setText("Recebido pelos Correios do Brasil");
//            } else if (finalstatus.equals("")) {
//                holder.time.setText("");
//            } else if (finalstatus.equals("Objeto postado após o horário limite da agência")) {
//                holder.time.setText("Postado após o horário limite da agência");
//            } else {
//                holder.time.setText("");
//            }
//
//
//            if (finalstatus.equals("Objeto postado")) {
//                holder.imageView.setImageResource(R.drawable.postado);
//            } else if (finalstatus.equals("Objeto encaminhado")) {
//                holder.imageView.setImageResource(R.drawable.encaminhado);
//            } else if (finalstatus.equals("Objeto saiu para entrega ao destinatário")
//                    || finalstatus.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio")) {
//                holder.imageView.setImageResource(R.drawable.saiu);
//            } else if (finalstatus.equals("Objeto entregue ao destinatário")) {
//                holder.imageView.setImageResource(R.drawable.success);
//            } else if (finalstatus.equals("Objeto aguardando retirada no endereço indicado")) {
//                holder.imageView.setImageResource(R.drawable.wait);
//            } else if (finalstatus.equals("Objeto recebido pelos Correios do Brasil")) {
//                holder.imageView.setImageResource(R.drawable.brasil);
//            } else if (finalstatus.equals("Objeto postado após o horário limite da agência")) {
//                holder.imageView.setImageResource(R.drawable.time);
//            } else if (finalstatus.equals("Destinatário Ausente")
//                    || finalstatus.equals("Destinatário não apresentou-se para receber")
//                    || finalstatus.equals("Destinatário recusou-se a receber")
//                    || finalstatus.equals("Destinatário desconhecido")
//                    || finalstatus.equals("Destinatário mudou-se")
//                    || finalstatus.equals("Destinatário não apresentou-se para receber")
//                    || finalstatus.equals("Destinatário ausente")
//                    || finalstatus.equals("Destinatário ausente em 3 tentativas de entrega")
//                    || finalstatus.equals("Destinatário não apresentou a documentação")) {
//                holder.imageView.setImageResource(R.drawable.nao_entregue);
//            } else if (finalstatus.equals("")) {
//                holder.imageView.setImageResource(R.drawable.null_reference);
//            } else {
//                holder.imageView.setImageResource(R.drawable.null_reference);
//            }
//
//        } catch (Exception ex) {
//
//        }
        return convertView;
    }

    public void delete(final String cod, final View v) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Remover objeto");
            builder.setMessage("Deseja remover o objeto selecionado?");
            builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dbController.removeData(cod);
                    Snackbar.make(v, "Objeto Removido", Snackbar.LENGTH_LONG).show();
//                    removed(1);
                    RastreioFragment.listViewRefresh();
                }
            }).setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();


        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
