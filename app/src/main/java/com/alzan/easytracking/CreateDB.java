package com.alzan.easytracking;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by Calazans on 21/08/2017.
 */

public class CreateDB extends SQLiteOpenHelper {


    public static final String BD = "SRODB994445";
    public static final String TABELA = "objetos";
    public static final String ID = "_id";
    public static final String COD = "codigo";
    public static final String ARCHIVE = "archive";
    public static final String STATUS = "status";
    public static final String TIME = "time";



    public static final String IDENTIFY = "Identify";
    public static final int VERSAO = 1;


    public CreateDB(Context mCtx) {
        super(mCtx, BD, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABELA + " ( " + ID + " INTEGER PRIMARY KEY ," +
                COD + " TEXT UNIQUE, " +
                ARCHIVE + " TEXT, " +
                STATUS + " TEXT, " +
                TIME + " TEXT, " +
                IDENTIFY + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA);
        onCreate(db);
    }
}
