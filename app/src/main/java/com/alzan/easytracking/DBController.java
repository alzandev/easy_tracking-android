package com.alzan.easytracking;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.alzan.easytracking.Entregues.ItemsEntregues;
import com.alzan.easytracking.Archive.ItemsArch;
import com.alzan.easytracking.Fragment.RastreioFragment;
import com.alzan.easytracking.Progress.ItemsProgress;
import com.alzan.easytracking.Rastreio.Items;

import java.util.ArrayList;

/**
 * Created by Calazans on 21/08/2017.
 */

public class DBController {
    private SQLiteDatabase db;
    private CreateDB feedReader;

    public DBController(Context context) {
        feedReader = new CreateDB(context);
    }

    public String insertData(String cod, String identify) {
        ContentValues values;
        long result;

        db = feedReader.getWritableDatabase();
        values = new ContentValues();
        values.put(CreateDB.COD, cod);
        values.put(CreateDB.IDENTIFY, identify);
        values.put(CreateDB.ARCHIVE, "false");

        try {
            result = db.insertOrThrow(CreateDB.TABELA, null, values);
            db.close();
            return String.valueOf(0);
        } catch (Exception ex) {
            return String.valueOf(ex.getMessage());
        }
    }

    public Cursor loadData() {
        Cursor cursor;
        String COMMA = ", ";
        String[] campos = {feedReader.ID};
        db = feedReader.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM " + feedReader.TABELA, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public ArrayList<Items> getData() {
        String COD = "codigo";
        String IDENTIFY = "Identify";
        String ARCHIVE = "archive";
        String STATUS = "status";


        db = feedReader.getReadableDatabase();
        ArrayList<Items> car = new ArrayList<>();
        Cursor result = db.rawQuery("select * from " + CreateDB.TABELA + " where archive = 'false'", null);
        while (result.moveToNext()) {
            car.add(new Items(result.getString(result.getColumnIndex(COD)),
                    result.getString(result.getColumnIndex(IDENTIFY)),
                    result.getString(result.getColumnIndex(ARCHIVE)),
                    result.getString(result.getColumnIndex(STATUS))));
        }
        return car;
    }

    public String getUnique() {
        String COD = "codigo";
        String IDENTIFY = "Identify";
        String FAV = "fav";
        String STATUS = "status";
        String TIME = "time";

        db = feedReader.getReadableDatabase();
        String result = null;
        Cursor cursor = db.rawQuery("select time from " + CreateDB.TABELA + " ORDER BY status DESC LIMIT 1", null);
        cursor.moveToNext();

        result = cursor.getString(cursor.getColumnIndex(TIME));

        return result;
    }

    public ArrayList<ItemsArch> favoriteData() {
        String COD = "codigo";
        String IDENTIFY = "Identify";
        String FAV = "fav";
        String STATUS = "status";

        db = feedReader.getReadableDatabase();
        ArrayList<ItemsArch> fav = new ArrayList<>();
        Cursor result = db.rawQuery("select * from " + CreateDB.TABELA + " where archive = 'true'", null);
        while (result.moveToNext()) {
            fav.add(new ItemsArch(result.getString(result.getColumnIndex(COD)), result.getString(result.getColumnIndex(IDENTIFY)),
                    result.getString(result.getColumnIndex(STATUS))));
        }
        return fav;
    }

    public ArrayList<ItemsEntregues> completeData() {
        String COD = "codigo";
        String IDENTIFY = "Identify";
        String FAV = "fav";
        String STATUS = "status";


        db = feedReader.getReadableDatabase();
        ArrayList<ItemsEntregues> car = new ArrayList<>();
        Cursor result = db.rawQuery("select * from " + CreateDB.TABELA + " WHERE status = 'Entrega Efetuada' AND archive = 'false'", null);
        while (result.moveToNext()) {
            car.add(new ItemsEntregues(result.getString(result.getColumnIndex(COD)),
                    result.getString(result.getColumnIndex(IDENTIFY)),
                    result.getString(result.getColumnIndex(STATUS))));
        }
        return car;
    }

    public ArrayList<ItemsProgress> progressData() {
        String COD = "codigo";
        String IDENTIFY = "Identify";
        String FAV = "fav";
        String STATUS = "status";


        db = feedReader.getReadableDatabase();
        ArrayList<ItemsProgress> car = new ArrayList<>();

        Cursor result = db.rawQuery("SELECT * FROM " + CreateDB.TABELA + " WHERE STATUS NOT IN " +
                "(SELECT STATUS FROM " + CreateDB.TABELA + " WHERE STATUS = " + "'Entrega Efetuada')", null);

        while (result.moveToNext()) {
            car.add(new ItemsProgress(result.getString(result.getColumnIndex(COD)),
                    result.getString(result.getColumnIndex(IDENTIFY)),
                    result.getString(result.getColumnIndex(STATUS))));
        }
        return car;
    }


    public String loadDataUnique(String cod) {
        return "OK";
    }

    public void removeData(String cod) {
        try {
            db = feedReader.getWritableDatabase();
            Cursor cursor = db.rawQuery("delete from " + feedReader.TABELA + " WHERE codigo = '" + cod + "'", null);
            cursor.moveToFirst();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateFavorite(String checked, String cod) {
        db = feedReader.getWritableDatabase();
        Cursor cursor = db.rawQuery("UPDATE " + feedReader.TABELA + " SET " + feedReader.ARCHIVE + " = '" + checked
                + "' WHERE " + feedReader.COD + " = '" + cod + "'", null);
        cursor.moveToFirst();
    }

    public void updateStatus(String status, String cod, String time) {
        db = feedReader.getWritableDatabase();
        Cursor cursor = db.rawQuery("UPDATE " + feedReader.TABELA + " SET " + feedReader.STATUS + " = '" + status
                + "', " + feedReader.TIME + " = '" + time + "' WHERE " + feedReader.COD + " = '" + cod + "'", null);
        cursor.moveToFirst();

//        String str2 = status;
//        String codigo = cod;
//
//
//        if (str2.equals("Objeto postado")) {
//            notification("Postado", codigo);
//        } else if (str2.equals("Objeto encaminhado")) {
//            notification("Encaminhado", codigo);
//        } else if (str2.equals("Objeto saiu para entrega ao destinatário")
//                || str2.equals("Objeto saiu para entrega ao destinat\\u00ef\\u00bf\\u00bdrio")
//                || str2.equals("Objeto saiu para entrega ao destinat�rio")) {
//            notification("Saiu%20para%20entrega%20ao%20destinatário", codigo);
//        } else if (str2.equals("Objeto entregue ao destinatário") || str2.equals("Objeto entregue ao destinatário? ?")
//                || str2.equals("Objeto entregue ao destinatário? ?") || str2.equals("Objeto entregue ao destinat�rio")) {
//            notification("Entrega%20Efetuada", codigo);
//        } else if (str2.equals("Objeto aguardando retirada no endereço indicado")) {
//            notification("Aguardando%20retirada", codigo);
//        } else if (str2.equals("Objeto recebido pelos Correios do Brasil")) {
//            notification("Recebido%20pelos%20Correios%20do%20Brasil", codigo);
//        } else if (str2.equals("Objeto postado após o horário limite da ag�ncia")
//                || str2.equals("Objeto postado após o horário limite da agência")
//                || str2.equals("Objeto postado ap�s o hor�rio limite da unidade")) {
//            notification("Postado%20após%20o%20horário%20limite%20da%20agência", codigo);
//        } else {
//            notification(str2, codigo);
//        }

//        RastreioFragment.listViewRefresh();
    }

    public String archive(String cod) {

        db = feedReader.getReadableDatabase();
        ArrayList<Items> arrayList = new ArrayList<>();

        db = feedReader.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + feedReader.ARCHIVE + " FROM "
                + feedReader.TABELA + " WHERE " + feedReader.COD + " = '" + cod + "'", null);
        cursor.moveToFirst();

        String result = cursor.getString(cursor.getColumnIndex(feedReader.ARCHIVE));

        return result;
    }
}
