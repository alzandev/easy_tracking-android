package com.alzan.easytracking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.alzan.easytracking.Fragment.CompleteFragment;
import com.alzan.easytracking.Fragment.ArquivadosFragment;
import com.alzan.easytracking.Fragment.ProgressFragment;
import com.alzan.easytracking.Fragment.RastreioFragment;
import com.alzan.easytracking.Others.AboutActivity;
import com.alzan.easytracking.Others.SettingsActivity;
import com.azimolabs.maskformatter.MaskFormatter;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String ADS_ID = "ca-app-pub-3657674781649044~7396441548";
        MobileAds.initialize(this, ADS_ID);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setCheckedItem(0);

        Fragment selectedFragment = RastreioFragment.newInstance();

        if (savedInstanceState == null) {
            navigationView.getMenu().performIdentifierAction(R.id.nav_box, 0);
        }

        MenuItem item = navigationView.getMenu().getItem(0);
        onNavigationItemSelected(item);

        navigationView.setNavigationItemSelectedListener(this);
        FirebaseMessaging.getInstance().subscribeToTopic("global");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment selectedFragment = RastreioFragment.newInstance();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_box) {
            setTitle("Todos");
            selectedFragment = RastreioFragment.newInstance();
            // Handle the camera action
        } else if (id == R.id.nav_fav) {
            setTitle("Arquivados");
            selectedFragment = ArquivadosFragment.newInstance();

        } else if (id == R.id.nav_progress) {
            setTitle("Em Andamento");
            selectedFragment = ProgressFragment.newInstance();

        } else if (id == R.id.nav_complete) {
            setTitle("Entregues");
            selectedFragment = CompleteFragment.newInstance();

        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, SettingsActivity.class));

        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutActivity.class));

        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, selectedFragment);
        transaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
